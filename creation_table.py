import connexion
import psycopg2
from psycopg2 import Error
import datetime

def createTable(connection):
    
    cursor = connection.cursor()

    crtable = """CREATE TABLE IF NOT EXISTS agregat_rss(
            id serial primary key,
            source text,
            title text,
            link text ,
            description text,
            pubdate text);"""

    cursor.execute(crtable)
    connection.commit()
createTable(connexion.connection_postgress())    
