import psycopg2
from psycopg2 import Error
import connexion


def deconnection(connection):
    cursor = connection.cursor()
    if connection != None:
        cursor.close()
        connection.close()
        print("PostgreSQL connection is closed")
    
