import psycopg2
from psycopg2 import Error
import tfk_feed_pars as tfkk
import app
import nettoyage_rss

src = 'https://www.reddit.com/r/dataisbeautiful/.rss'

#print(tfkk.t_feed_parse(src))

def insertionTables(connection, url):
    
    cursor = connection.cursor()

    postgres_insert_query = """INSERT INTO agregat_rss(source,title,pubdate,link,description)VALUES(%s,%s,%s,%s,%s)"""

    #filess = tfk_feed_pars.t_feed_parse(tfk_feed_pars.src,tfk_feed_pars.src1,tfk_feed_pars.src2,tfk_feed_pars.src3,tfk_feed_pars.src4)

    filess = nettoyage_rss.source(url)
    
    for elt in filess:
        record = (elt['source'],elt['title'],elt['pubdate'],elt['link'],elt['description'])
        cursor.execute(postgres_insert_query, record)
    connection.commit()
    
        
    print("insertion faite avec sucess")

def suppression_doublon(connection):

    cursor = connection.cursor()

    postgres_delete_query = """DELETE FROM agregat_rss a USING agregat_rss b WHERE a.id > b.id AND a.title = b.title;"""
 
    cursor.execute(postgres_delete_query)

    connection.commit()


def Suppr_source(connection, check):

    cursor = connection.cursor()
    
    postgres_supp_query = f" DELETE FROM agregat_rss WHERE source = '{check.strip()}';"
    print(postgres_supp_query)
    cursor.execute(postgres_supp_query)

    connection.commit()
#if __name__=="__main__":
#    insertionTables(connexion.connection_postgress())
#    suppression_doublon(connexion.connection_postgress())