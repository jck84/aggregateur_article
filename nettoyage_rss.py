import tfk_feed_pars
from datetime import datetime

def source(url):
    # recuperation de la source2
    flux = tfk_feed_pars.t_feed_parse(url)

    liste_date = []
    # valeurs du dico avec key pubdate dans une liste
    for elem in flux:
        liste_date.append(elem['pubdate'])
    
    
    # suppression des 5 premiers characteres
    listes = []
    for elem in liste_date:
        listes.append(elem[5:])
    
    
    listess = []
    for elem in listes:
        if ' +0000' in elem:
            listess.append(elem.replace(' +0000', ''))
        if ' +0100' in elem:
            listess.append(elem.replace(' +0100', ''))
        if ' GMT' in elem:
            listess.append(elem.replace(' GMT', ''))

#    final_date = []

#    for elem in listess:
#        dates = (datetime.strptime(elem, '%d %b %Y %H:%M:%S'))
#        final_date.append(dates)


    i = 0
    for elem in flux:
        elem['pubdate'] = listess[i]
        i = i + 1
    

    # recuperation des valeurs description dans une liste
    liste_desc = []
    for elem in flux:
        liste_desc.append(elem['description'])
    # suppresion des 14 derniers characteres
    liste_description = []
    for elem in liste_desc:
        liste_description.append(elem.replace('">Comments</a>', ''))

    # suppression du debut du string en trop
    liste_descrip = []
    for elem in liste_description:
        liste_descrip.append(elem.replace('<a href="', ''))
    # rajout des valeurs description nettoyer dans le dico

    liste_descript = []
    for elem in liste_descrip:
        liste_descript.append(elem.replace('">Read the full story</a></p>', ''))

    liste_descripti = []
    for elem in liste_descript:
        liste_descripti.append(elem.replace('<p>', ''))

    i = 0
    for elem in flux:
        elem['description'] = liste_descripti[i]
        i = i + 1

    liste_source = []
    for elem in flux:
        liste_source.append(elem['source'])

    liste_sources = []
    for elem in liste_source:
        liste_sources.append(elem.replace('https://', ''))

    liste_sourcess = []
    for elem in liste_sources:
        liste_sourcess.append(elem.replace('http://', ''))

    liste_sourcesss = []
    for elem in liste_sourcess:
        liste_sourcesss.append(elem.replace('www.', ''))

    a = []
    for elem in liste_sourcesss:
        a.append(elem.split('/'))
        
    b = []
    for elem in a:
        b.append(elem[0])

    i = 0
    for elem in flux:
        elem['source'] = b[i]
        i = i + 1
    
    return flux

