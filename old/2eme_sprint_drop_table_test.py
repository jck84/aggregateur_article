import psycopg2
from psycopg2 import Error
import connexion

    
def supression(connection):
    cursor = connection.cursor()
    
    postgres_drop_query = """ DROP TABLE TEST;"""
      
    cursor.execute(postgres_drop_query)

    connection.commit()
    count = cursor.rowcount
    print (count, "table deleted successfully")

if __name__=="__main__":
    supression(connexion.connection_postgress())