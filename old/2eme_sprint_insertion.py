import psycopg2
from psycopg2 import Error
import tfk_feed_pars
import connexion

    
def insertion(connection):
    cursor = connection.cursor()
    
    postgres_insert_query = """ INSERT INTO RSS (title,pubdate,link,description) VALUES (%s,%s,%s,%s)"""
    

    files = tfk_feed_pars.t_feed_parse(tfk_feed_pars.src2)
    for elem in files:
        record = [elem['title'],elem['pubdate'],elem['link'],elem['description']]
        cursor.execute(postgres_insert_query, record)

    connection.commit()
    count = cursor.rowcount
    print (count, "Record inserted successfully into table")


if __name__=="__main__":
    insertion(connexion.connection_postgress())